import React from 'react';
import { Gyroscope } from 'expo';
import { StyleSheet, Dimensions, Text, View, Image } from 'react-native';

export default class App extends React.Component {
  state = {
    gyroscope: {},
    rotation: 200,
    newData: false,
    windowDims: null
  }

  componentDidMount() {
    this._subscribe();
    Gyroscope.setUpdateInterval(100)
  }

  componentWillMount() {
    Dimensions.addEventListener("change", this._dimensionsHandler)
  }

  componentDidUpdate() {
    const { rotation, gyroscope, newData } = this.state

    if (gyroscope.z > 0 && newData === true) {
      this.setState({
        rotation: rotation + parseInt((gyroscope.z.toFixed(1) * 180 / Math.PI).toFixed(0)) / 10,
        newData: false
      })
    } else if (gyroscope.z < 0 && newData === true) {
      this.setState({
        rotation: rotation + parseInt((gyroscope.z.toFixed(1) * 180 / Math.PI).toFixed(0)) / 10,
        newData: false
      })
    }
  }

  componentWillUnmount() {
    this._unsubscribe()
    Dimensions.removeEventListener("change", this._dimensionsHandler)
  }

  _subscribe = () => {
    this._subscription = Gyroscope.addListener((result) => {
      this.setState({
        gyroscope: result,
        newData: true
      });
    });
  }

  _unsubscribe = () => {
    this._subscription && this._subscription.remove()
    this._subscription = null;
  }

  _dimensionsHandler = (dims) => {
    this.setState({ windowDims: dims })
  }

  render() {
    const { rotation, gyroscope, windowDims } = this.state
    let infoTextLeft = 100
    let circleDeg = 0

    if (windowDims !== 'undefined' && windowDims !== null) {
      const mode = windowDims.window.height > windowDims.window.width ? "portrait" : "landscape"

      if (mode === "landscape") {
        infoTextLeft = 100
        circleDeg = `-90`
      } else {
        infoTextLeft = (windowDims.width / 2)
        circleDeg = 0
      }
    }

    return (
      <View style={styles.container}>
        <View style={styles.bgWrapper}>
          <Image source={require("./assets/gradient.png")} style={styles.bgImage} />
        </View>
        <Image source={require("./assets/circle.png")} style={{ width: 315, height: 316, position: 'absolute', transform: [{ rotate: `${circleDeg}deg` }] }} />
        <Image source={require("./assets/direction.png")} style={{ width: 27, height: 177, transform: [{ rotate: `${rotation - -circleDeg}deg` }] }} />
        <View style={{ position: 'absolute', top: 100, left: infoTextLeft }}>
          <Text style={{ textAlign: 'center', color: '#fff' }}>Rotation:</Text>
          <Text style={{ textAlign: 'center', color: '#fff', fontSize: 20 }}>{rotation.toFixed(1)}deg</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ACDDE8',
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  bgImage: {
    backgroundColor: '#ccc',
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
