React Native Compass
===============================

### Installation

First, run:
```
npm install
```

When it's ready, run:
```
yarn start
```

(You need Expo app to test this)